CREATE TABLE bookdetails (
    book_Id bigint(25) NOT NULL AUTO_INCREMENT,
    author varchar(255),
    category varchar(255),
    description varchar(255),
    PRIMARY KEY(book_Id)
);