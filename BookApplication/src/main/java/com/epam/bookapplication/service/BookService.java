package com.epam.bookapplication.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.bookapplication.data.Book;
import com.epam.bookapplication.exception.BookException;
import com.epam.bookapplication.model.BookDTO;
import com.epam.bookapplication.repository.BookRepository;

@Service
public class BookService {
	@Autowired
	private BookRepository bookRepository;
	@Autowired
	private ModelMapper modelmapper;

	Book book;
	public List<BookDTO> getAllBooks() {
		return modelmapper.map(bookRepository.findAll(), new TypeToken<List<BookDTO>>() {
		}.getType());
	}

	public BookDTO addBook(int bookId, String author, String category, String description) throws BookException {
		if(!(bookRepository.findById(bookId).isEmpty())) {
			throw new BookException("Book Already exists with specified Id");
		}
		if(author.isEmpty()) {
			throw new BookException("Invalid Author Name");
		}
		if(category.isEmpty()) {
			throw new BookException("Invalid Category");
		}
		if(description.isEmpty()) {
			throw new BookException("Invalid Description");
		}
		book = new Book();
		book.setAuthor(author);
		book.setCategory(category);
		book.setDescription(description);
		return modelmapper.map(bookRepository.save(book), BookDTO.class);
	}

	public BookDTO getBookById(int bookId) throws BookException {
		if(bookRepository.findById(bookId).isEmpty()) {
			throw new BookException("BookId is invalid");
		}
		return modelmapper.map(bookRepository.getById(bookId), BookDTO.class);
	}

	public BookDTO updateBook(int bookId, String author, String category, String description) throws BookException {
		if(bookRepository.findById(bookId).isEmpty()) {
			throw new BookException("BookId is invalid");
		}
		if(author.isEmpty()) {
			throw new BookException("Invalid Author Name");
		}
		if(category.isEmpty()) {
			throw new BookException("Invalid Category");
		}
		if(description.isEmpty()) {
			throw new BookException("Invalid Description");
		}
		book = bookRepository.getById(bookId);
		book.setAuthor(author);
		book.setCategory(category);
		book.setDescription(description);
		return modelmapper.map(bookRepository.save(book), BookDTO.class);
	}

	public boolean deleteBook(int bookId) throws BookException {
		if(bookRepository.findById(bookId).isEmpty()) {
			throw new BookException("Invalid BookId");
		}
		bookRepository.deleteById(bookId);
		return !bookRepository.findById(bookId).isPresent();
	}
}
