package com.epam.bookapplication.model;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class BookDTO {
	private int bookId;
	private String author;
	private String category;
	private String description;
}
