package com.epam.bookapplication.exception;

import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.http.HttpStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BookException extends RuntimeException {
	public BookException(String msg) {
		super(msg);
	}

}
