package com.epam.bookapplication.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.bookapplication.data.Book;


public interface BookRepository extends JpaRepository<Book,Integer>{
	
}
