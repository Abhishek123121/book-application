package com.epam.bookapplication.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.epam.bookapplication.exception.BookException;
import com.epam.bookapplication.model.BookDTO;
import com.epam.bookapplication.service.BookService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

@RestController
public class BookRestController {
	@Autowired
	private BookService bookService;

	@GetMapping("/books")
//	@HystrixCommand(fallbackMethod = "fallbackOvertime", commandProperties = {
//			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "1000") })
	public ResponseEntity<List<BookDTO>> getAllBooks(){
		return new ResponseEntity<>(bookService.getAllBooks(), HttpStatus.OK);
	}
	
	@GetMapping("/books/{bookId}")
	public ResponseEntity<BookDTO> getBookById(@PathVariable int bookId) throws BookException {
		return new ResponseEntity<BookDTO>(bookService.getBookById(bookId), HttpStatus.OK);
	}

	@PostMapping("/books")
	public ResponseEntity<BookDTO> addBook(@RequestBody BookDTO bookDTO) throws BookException {
		return new ResponseEntity<BookDTO>(bookService.addBook(bookDTO.getBookId(), bookDTO.getAuthor(),
				bookDTO.getCategory(), bookDTO.getDescription()), HttpStatus.OK);
	}

	@PutMapping("/books/{bookId}")
	public ResponseEntity<BookDTO> updateBook(@PathVariable int bookId, @RequestBody BookDTO bookDTO)
			throws BookException {
		return new ResponseEntity<BookDTO>(
				bookService.updateBook(bookId, bookDTO.getAuthor(), bookDTO.getCategory(), bookDTO.getDescription()),
				HttpStatus.OK);
	}

	@DeleteMapping("/books/{bookId}")
	public ResponseEntity<Boolean> deleteBook(@PathVariable int bookId) throws BookException {
		return new ResponseEntity<Boolean>(bookService.deleteBook(bookId), HttpStatus.OK);
	}

}
